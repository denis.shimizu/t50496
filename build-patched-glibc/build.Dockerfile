# Start with a Debian base image
FROM debian:bookworm

RUN apt-get update && apt-get upgrade -y
#### Build v4l2_stateful_decoder ####
# Install dependencies
RUN apt-get update && apt-get install -y \
  git \
  python3 \
  curl \
  lsb-release \
  sudo \
  vim \
  wget \
  gnupg \
  gperf \
  autoconf \
  automake \
  libtool \
  make \
  cmake \
  pkg-config \
  python-is-python3 \
  ninja-build \
  clang \
  apt-utils \
  && rm -rf /var/lib/apt/lists/*

# Install depot_tools
RUN git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git /depot_tools
ENV PATH="/depot_tools:${PATH}"

RUN apt-get update && apt install -y libglib2.0-0

#### Patch glibc ####
# Install necessary packages for building glibc
RUN apt-get update && apt-get install -y build-essential \
  wget gawk git libc6-dev dpkg-dev debhelper devscripts lsb-release \
  linux-libc-dev libcrypt-dev libnsl-dev rpcsvc-proto 

# Create /etc/apt/sources.list with standard Debian repositories including source repos
RUN echo "deb-src http://deb.debian.org/debian $(lsb_release -cs) main" >> /etc/apt/sources.list && \
  echo "deb-src http://deb.debian.org/debian-security $(lsb_release -cs)-security main" >> /etc/apt/sources.list && \
  echo "deb-src http://deb.debian.org/debian $(lsb_release -cs)-updates main" >> /etc/apt/sources.list

# Update package lists to include sources
RUN apt-get update && apt-get install -y \
  quilt rdfind symlinks gperf bison libaudit-dev libcap-dev libselinux1-dev binutils-for-host g++-12-multilib libgd-dev

# Identify the exact version of glibc from the installed libc6
RUN GLIBC_VERSION=$(dpkg-query -W -f='${Version}' libc6 | sed 's/[^0-9.]*//g') && \
  echo "Glibc version: $GLIBC_VERSION" && \
  mkdir -p /usr/src/glibc && \
  cd /usr/src/glibc && \
  apt-get source glibc=$(apt-cache show libc6 | grep -oP '(?<=Version: )'"$GLIBC_VERSION"'(-[^ ]+)?' | head -1)

# Copy the patch file to the Docker image
COPY 0001-Revert-Add-GLIBC_ABI_DT_RELR-for-DT_RELR-support.patch /usr/src/glibc/patch-file.patch

# Apply the patch
RUN cd /usr/src/glibc/glibc-* && \
    patch -p1 < /usr/src/glibc/patch-file.patch

# Build and install the patched glibc
RUN cd /usr/src/glibc/glibc-* && \
  DEB_BUILD_OPTIONS=nocheck dpkg-buildpackage -b -uc

RUN cd /usr/src/glibc && \
  dpkg -i *.deb && \
  tar -cvf patched_glibc.tar *.deb

# # Clean up unnecessary files
# RUN apt-get clean && \
#   rm -rf /var/lib/apt/lists/* /usr/src/glibc

# ENTRYPOINT cd /chromium/src && \
#   ninja -C out/arm64 v4l2_unittest