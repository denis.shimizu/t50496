#!/bin/bash
set -e
PROJECT="my-build"

### If targetting aarch64, uncomment the following line##
docker run --rm -it --privileged docker/binfmt:a7996909642ee92942dcd6cff44b9b95f08dad64 && \

docker build -f build.Dockerfile -t $PROJECT . && \
docker run -it --rm \
  -v $HOME/chromium-v4l2:/chromium \
  --name $PROJECT \
  --network host \
  $PROJECT