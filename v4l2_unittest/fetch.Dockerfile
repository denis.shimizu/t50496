# Start with a Debian base image
FROM debian:bookworm

RUN apt-get update && apt-get upgrade -y
#### Build v4l2_stateful_decoder ####
# Install dependencies
RUN apt-get update && apt-get install -y \
  git \
  python3 \
  curl \
  lsb-release \
  sudo \
  vim \
  wget \
  gnupg \
  gperf \
  autoconf \
  automake \
  libtool \
  make \
  cmake \
  pkg-config \
  python-is-python3 \
  ninja-build \
  clang \
  apt-utils \
  && rm -rf /var/lib/apt/lists/*

# Install depot_tools
RUN git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git /depot_tools
ENV PATH="/depot_tools:${PATH}"

# Fetch Chromium source and checkout specific commit
RUN mkdir /chromium && cd /chromium && \
  fetch --nohooks --nohistory chromium

RUN cd /chromium/src && ./build/install-build-deps.sh --no-prompt && \
  gclient runhooks && mkdir -p out/arm64

RUN cd /chromium/src && \
  build/linux/sysroot_scripts/install-sysroot.py --arch=arm64 && \
  gn gen out/arm64 --args='target_cpu="arm64" use_v4l2_codec=true'

ENTRYPOINT cp -r /chromium/. /chromium-copy